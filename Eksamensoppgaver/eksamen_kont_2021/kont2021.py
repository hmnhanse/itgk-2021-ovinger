# Denne filen er et kodeskjelett som du kan bruke som basis for oppgavene
# i del 3 av eksamen. Det er ikke sikkert at oppgavebeskrivelsen her er
# helt perfekt - les beskrivelsen på Inspera for å være sikker.


'''
Del 3

Oppgaven dreier seg om registrering av skader på elsparkesykler for en del norske byer.
Alle data lagres i en dictionary kalt register. Bynavnet fungerer som nøkkel.
Til hver by er det knyttet en todimensjonal liste. Hvert element i denne listen
er igjen en liste, med dagnummer og antall registrerte skader, som vist her:

{'Oslo': [[1, 28], [2, 38], [3, 29], ...],
'Bergen': [[1, 47], [2, 16], [3, 20], ...],
...
'Kristiansand': [[1, 6], [2, 5], [3, 1], ...]}

Du kan se bort ifra eventuelle år og slikt.

Del 3 er delt i et sett med deloppgaver, der noen deloppgaver lettere kan løses
hvis en bruker funksjoner skrevet i tidligere deloppgaver. Det er viktig å huske
at en kan kalle funksjoner fra tidligere deloppgaver selv om en ikke har fått dem
til helt.
'''



'''
Oppgave a:
Skriv funksjonen antall_minimum_skader(registrerte_skader, minimum).

Parametre: en liste med registrerte skader på elsparkesykler og en minimumsverdi minimum (heltall).
Returnerer: antall elementer i listen som har en verdi på minst minimum

Eksempel: Hvis registrerte_skader er [12, 11, 14, 16, 22, 27, 30, 22, 29, 19] og minimum er 20, så vil funksjonen returnere 5.
'''



#registrerte_skader = [20 + random.randint(-10,30) for i in range(10)]
#print(f'{registrerte_skader}: {antall_minimum_skader(registrerte_skader, 20)}')

'''
Oppgave b:
Skriv funksjonen skriv_til_fil(by, dag, skader)
Parametre: by (streng), dag (heltall), skader (heltall)
Formål: Funksjonen skal legge verdiene den får til som en ny linje på slutten av filen 'registreringer.txt' i samme folder som pythonprogrammet.
Eksempel:
Linjen i filen skal se slik ut ved kjøring av skriv_til_fil('Oslo', 1, 28):
Oslo, 1, 28
'''



#skriv_til_fil('Oslo', 1, 28):

'''
Oppgave c:
Skriv en funksjon hent_skader(dato_skader) som tar inn en todimensjonal liste, dato_skader, 
på formen [[1,28],[2,38],[3,29],...]. Funksjonen skal returnere en ny liste med det andre elementet 
fra alle sublistene til dato_skader. I eksempelet over vil denne da returnere [28,38,29,...]
'''



#print(hent_skader([[1,28],[2,38],[3,29]]))

'''
Oppgave d:
Skriv funksjonen legg_til_dag(register, by, dag, skader).
Parametre:
    - register er en dictionary med struktur som oppgitt i starten av del 3.
    - by er navnet på byen. 
    - dag og skader er heltall. 

Formål: Funksjonen skal legge til et element bakerst i listen for den gitte byen. Elementet
er en ny liste som inneholder dag og skader på formen [dag, skader]. Hvis byen ikke er registert tidligere
må dette håndteres. Du kan forvente at alle linjer er korrekte.
Retur: Funksjonen skal returnere det oppdaterte registeret.
'''



#register = {}
#register = legg_til_dag(register, 'Oslo', 1, 28)
#print(f'register: {register})

'''
Oppgave e:
Skriv funksjonen les_registreringer(). Funksjonen skal lese inn et sett
med linjer fra filen 'registreringer.txt'. Hver linje har formatet
By, dag, skader. De første fire linjene er:

Oslo, 1, 28
Bergen, 1, 47
Trondheim, 1, 24
Stavanger, 1, 31


Hver linje skal legges til i en dictionary. Nøkkelen skal være byen.
Dag og skader skal legges til som det siste elementet i denne byens
liste.
Funksjonen skal returnere den oppdaterte dictionaryen.

Husk at du kan bruke funksjoner som allerede er laget!
'''



# register = les_registreringer()

'''
Oppgave f:
Skriv funksjonen finn_flest_skader(register, minimum).
Funksjonen tar inn en dictionary av struktur som forklart i starten av del 3, samt en minimumsverdi (heltall).
Denne verdien er det minste antall skader som skal være med i tellingen. Du kan
forvente at alle dager er registrert, og at listen starter med dag 1.
Funksjonen skal returnere en streng som eksempelvis ser slik ut:

Oslo har flest dager med over 60 skader, med 73 dager.

Hvis flere byer vinner, så skal det skrives ut som:

Oslo og Bergen har flest dager over 60 skader, med 73 dager.

Husk at du kan benytte deg av funksjoner skrevet før! Også hvis du ikke
har klart å lage dem helt. Du kan da 'forvente' at de er korrekte.

'''



#finn_flest_skader(register, 60)

'''
Oppgave g:
Skriv funksjonen snitt_skader(register, startdag, sluttdag)
Funksjonen skal beregne gjennomsnitt antall skader mellom (og inkludert) to gitte dager.
Register er datastrukturen beskrevet i starten av del 3, startdag og sluttdag er heltall.
Du kan anta at startdag har lavere verdi enn sluttdag.

Disse skal så skrives ut sortert etter byenes navn (alfabetisk) som vist under.
Du kan forvente at alle dager er registrert, og at listen starter med dag 1.
Gjennomsnittsverdien skal skrives ut med ett desimal.

Eksempel:
>>> snitt_skader(register, 200, 220)
Bergen: 37.4
Drammen: 17.1
Kristiansand: 5.6
Oslo: 42.1
Stavanger: 19.8
Tromsø: 10.5
Trondheim: 24.1
'''


#snitt_skader(register, 200, 220)
